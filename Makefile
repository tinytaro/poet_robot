.PHONY: all clean

#CC=arm-linux-gnueabi-gcc
CC=gcc
CFLAGS = -Wall -D_GNU_SOURCE
LDLIBS = $(shell curl-config --libs)

all: poet

poet: poet.c cJSON.c

clean:
	$(RM) poet

# 自动作诗

#### 介绍
演示百度创作智能写诗API调用

#### 使用说明
1.  安装libcurl-dev软件包，`apt install libcurl4-gnutls-dev`
2.  将`poet.c`中的`APPID`和`USERID`替换成自己的。
3.  make

API文档：https://ai.baidu.com/docs#/IntelligentWriting-API/top

#### 运行
./poet 华清远见

        华清远见
     华清池水漾心波
     远见渔舟唱晚歌
     野老不知何处去
     林深树密鸟声多